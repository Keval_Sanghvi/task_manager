const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const routes = require('./routes/router');
const connectDB = require('./db/connect');
const notFound = require('./middleware/not-found');
const errorHandlerMiddleware = require('./middleware/error-handler');

const app = express();

app.use(morgan('tiny'));
app.use(express.static('./public'));
app.use(express.json());

app.use('/api/v1/tasks', routes);

app.use(notFound);
app.use(errorHandlerMiddleware);

dotenv.config({path: '.env'});
const PORT = process.env.PORT || 8888;

const start = async () => {
  try {
    await connectDB(process.env.dbURI);
    app.listen(PORT, () =>
      console.log(`Server is listening on PORT ${PORT}...`)
    );
  } catch (error) {
    console.log(error);
  }
};

start();
